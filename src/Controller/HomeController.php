<?php

namespace App\Controller; 

use App\Entity\User;
use App\Entity\Animaux;
use App\Entity\Contact;
use App\Form\ContactType;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);


    }
     
/** 
     * @Route("/animaux", name="animaux")
     */
    public function animaux(): Response
    {

        return $this->render('home/animaux.html.twig', [
           
        ]);
    }


    /** 
     * @Route("/services", name="services")
     */
    public function services(): Response
    {
        return $this->render('home/services.html.twig', [
           
        ]);
    }


     /** 
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, ObjectManager $manager): Response
    {

        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($contact);
            $manager->flush();

            $this->addFlash(
                'success',
                "Message envoyé !"
            );

            return $this->redirectToRoute('contact');
        }
        return $this->render('home/contact.html.twig', [
            'formulaire' => $form->createView()
        ]);
    }
}




