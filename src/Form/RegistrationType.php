<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationType extends AbstractType

{
    
        /**
         * Permet d'avoir la configuration de base d'un champ !
         * 
         * @param string $label
         * @param string $placeholder
         * @param array $options
         * @return array
         */
            private function getConfiguration($label, $placeholder, $options = []){
                return array_merge([
                    'label' => $label,
                    'attr' => [
                        'placeholder' => $placeholder
                    ]
                    ], $options);
            }
        
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, $this->getConfiguration("Email", "Email personnel"))
            ->add('password', PasswordType::class, $this->getConfiguration("Password", "mot de passe"))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}